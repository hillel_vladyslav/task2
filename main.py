def my_range(start: int, stop: int, step=1):
    while start < stop:
        yield start
        start = start + step


index = 0
for i in my_range(1, 10):
    print(f"Output #{index} Number: {i}")
    index += 1
